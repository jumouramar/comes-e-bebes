<?php
    get_header();
?>
<main>

        <section class="banner">
            <h2>Comes&Bebes</h2>
            <h3>O restaurante para todas as fomes</h3>
        </section>
        <div class="main-content">
            <h2>CONHEÇA NOSSA LOJA</h2>
    
            <div class="container-categories">
                 <p>Tipos de pratos principais</p>
    
                <!-- Categorias -->
                <div class="categories">
                    <?php
                    do_action( 'available_categories', 'mostrar_categorias');
                    ?>
                </div>
            </div>
    
            <div class="menu-day">
                <div class="day">
                    <p>Pratos do dia de hoje:</p>
                    <h3>
                        <?php
                            /*
                            $day = ucfirst(strftime('%A', strtotime('today')));
                            $length = strpos($day,"-feira");
                            echo substr($day, 0, $length);*/
                            setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
                            date_default_timezone_set('America/Sao_Paulo');
                            $diasemana = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');
                            $data = date('Y-m-d');
                            $diasemana_numero = date('w', strtotime($data));
                            echo $diasemana[$diasemana_numero];
                        ?>
                    </h3>
                </div>
                <!-- Pratos -->
                <div class="dishes-list">
                    <?php
                        $produtos = wc_get_products([
                            'limit' => 4,
                            'orderby' => 'rand'
                        ]);
    
                        foreach($produtos as $produto){
                            formatar_produto($produto);
                        }
                    ?>
                </div>
            </div>
    
            <a class="go-store" href="http://localhost:10009/shop/">Veja outras opções</a>
        </div>
    
        <section class="visite">
            <h2>Visite a nossa loja física</h2>
            <div class="container_localizacao">
                <div class="mapa" style="overflow:hidden;width: 700px;position: relative;">
                    <iframe width="345" height="206" src="https://maps.google.com/maps?width=700&amp;height=440&amp;hl=en&amp;q=Av.%20Gal.%20Milton%20Tavares%20de%20Souza%2C%2C%20s%2Fn%20-%20S%C3%A3o%20Domingos%2C%20Niter%C3%B3i%20-%20RJ%2C%2024210-310+(T%C3%ADtulo)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><div style="position: absolute;width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;"><small style="line-height: 1.8;font-size: 2px;background: #fff;">Powered by <a href="https://embedgooglemaps.com/fr/">embedgooglemaps FR</a> & <a href="https://harpangratis.se/">harpan</a></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
                </div>   
                <p class="localizacao_info" id="endereco"><span class='icones endereco'>Av. Gal. Milton Tavares de Souza</p>
                <p class="localizacao_info" id="telefone"><span class='icones telefone'>(21)9999 - 9999</p>
            </div>
            <div class="slideshow">
    
                <div class="slides">
                    <div class="slide"></div>
                    <div class="slide"></div>
                    <div class="slide"></div>
                </div>
                <button class="slider"></button>
                <button class="slider"></button>
                <button class="slider"></button>
            </div>
    
        </section>
</main>
<?php
    get_footer();
?>