//Escolher quantidade do produto
var buttonDecrease = document.querySelector('.button-quantity-decrease');
var buttonIncrease = document.querySelector('.button-quantity-increase');
var quantity = document.querySelector('.qty');

quantity.value = 0;

function increase(){
    var currenteValue = parseInt(quantity.value);
    currenteValue += 1;
    quantity.value = currenteValue;
}

function decrease(){
    var currenteValue = parseInt(quantity.value);
    if (currenteValue > 0){
        currenteValue -= 1;
        quantity.value = currenteValue;  
    }
    
}

buttonDecrease.addEventListener("click", decrease);
buttonIncrease.addEventListener("click", increase)
