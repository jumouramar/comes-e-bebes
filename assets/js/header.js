function openMenu(){
    let menu = document.querySelector(".burguer-menu-content")
    menu.style.display = "block"
}

function closeMenu(){
    let menu = document.querySelector(".burguer-menu-content")
    menu.style.display = "none"
}

document.querySelector(".burguer-menu").addEventListener("mouseover", openMenu);

document.querySelector(".header").addEventListener("mouseleave", closeMenu);