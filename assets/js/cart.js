function openMenu(){
    console.log("click")
    let menu = document.querySelector(".lateral-cart")
    menu.style.display = "block"
}

function closeMenu(){
    let menu = document.querySelector(".lateral-cart")
    menu.style.display = "none"
}

document.querySelector(".cart-icon").addEventListener("click", openMenu);

document.querySelector(".link-cart").addEventListener("click", openMenu);

document.querySelector(".x-icon").addEventListener("click", closeMenu);