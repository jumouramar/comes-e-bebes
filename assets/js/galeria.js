window.addEventListener('DOMContentLoaded', () => {
    var imagens_galeria = document.querySelectorAll('.slide');
    var btns = document.querySelectorAll('.slider');

    
    btns.forEach((btn,indexBtn) => btn.addEventListener('click',()=>{
        mudarImagem(indexBtn);
    }))

    function mudarImagem(index){
        imagens_galeria.forEach((imagem,indexImagem)=>{
            if(indexImagem != index){
                imagem.style.display = "none";
            }else{
                imagem.style.display = "initial";
            }
        })
    }
})