<?php 

function mostrar_categorias(){
    $args = array(
           'taxonomy'  => 'product_cat',
           'orderby' => 'name',
    );
    
    $lista_categorias = [];
    $categorias = get_categories($args);

    foreach($categorias as $categoria){

        if($categoria->name != 'Uncategorized'){

            $id_categoria = $categoria->term_id;
            $id_img = get_term_meta($id_categoria, 'thumbnail_id', true);

            $lista_categorias[] = [
            'name' => $categoria->name,
            'id'=> $id_categoria,
            'link'=> get_term_link($id_categoria, 'product_cat'),
            'img'=> wp_get_attachment_image_src($id_img, 'slide')[0]
            ];
            };

        };
    
        foreach($lista_categorias as $categoria_exibida){
            $css_image =  'background-image: url('. $categoria_exibida['img'] .')';
            ?>
            <a class="category" href = '<?= $categoria_exibida['link'];?>' style="<?= $css_image;?>">
                <h3><?= $categoria_exibida['name']?></h3>
            </a>
            <?php
    }
};

?>