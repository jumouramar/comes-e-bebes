<?php 

function get_selected_category(){
    global $wp;
    $urlCategory = home_url($wp->request);

    $args = array(
        'taxonomy'  => 'product_cat',
        'orderby' => 'name',
    );

    $categorias = get_categories($args);

    foreach($categorias as $categoria){
        $nameCategory = $categoria->slug;
        
        if(str_contains($urlCategory, $nameCategory)){
            if($nameCategory == 'massa'){
                return 'MASSA';
            }
            else{
                return 'COMIDA ' . strtoupper($nameCategory);
            }
        }
    }
}

?>