<?php

function comes_e_bebes_enqueue_style(){
    wp_register_style('comes-e-bebes-reset', STYLES_DIR . '/reset.css', [], '1.0.0', false);
    wp_register_style(
        'comes-e-bebes-fonts',
        "https://fonts.googleapis.com/css2?family=Bellota+Text:wght@300;400;700&family=Comic+Neue:wght@700&family=Roboto:wght@300;400;500;900&display=swap"
    );
    wp_register_style('style', get_stylesheet_uri(), [], '1.0.0', false);
    wp_register_style('comes-e-bebes-header', STYLES_DIR . '/header.css', [], '1.0.0', false);
    wp_register_style('page-home', STYLES_DIR . '/pagehome.css', [], '1.0.0', false);
    wp_register_style('comes-e-bebes-footer', STYLES_DIR . '/footer.css', [], '1.0.0', false);
    wp_register_style('comes-e-bebes-nav', STYLES_DIR . '/nav.css', [], '1.0.0', false);
    wp_register_style('comes-e-bebes-archive', STYLES_DIR . '/archive-product.css', [], '1.0.0', false);
    wp_register_style('comes-e-bebes-adress', STYLES_DIR . '/adress.css', [], '1.0.0', false);
    wp_register_style('comes-e-bebes-cart', STYLES_DIR . '/cart.css', [], '1.0.0', false);
    wp_register_style('comes-e-bebes-painel', STYLES_DIR . '/painel.css', [], '1.0.0', false);
    wp_register_style('comes-e-bebes-nav-myaccount', STYLES_DIR . '/nav-myaccount.css', [], '1.0.0', false);
    wp_register_style('comes-e-bebes-orders', STYLES_DIR . '/orders.css', [], '1.0.0', false);
    wp_register_style('comes-e-bebes-single-product', STYLES_DIR . '/single-product.css', [], '1.0.0', false);
    wp_register_style('comes-e-bebes-checkout', STYLES_DIR . '/checkout.css', [], '1.0.0', false);


    wp_register_script('comes-e-bebes-header-js', SCRIPTS_DIR . '/header.js', [], '1.0.0', true);
    wp_register_script('comes-e-bebes-galeria-js', SCRIPTS_DIR . '/galeria.js', [], '1.0.0', true);
    wp_register_script('comes-e-bebes-search-dishe-js', SCRIPTS_DIR . '/search_dishe.js', [], '1.0.0', true);
    wp_register_script('comes-e-bebes-cart-js', SCRIPTS_DIR . '/cart.js', [], '1.0.0', true);
    wp_register_script('comes-e-bebes-simple-product-js', SCRIPTS_DIR . '/simple_product.js', [], '1.0.0', true);
    wp_register_script('comes-e-bebes-variable-product-js', SCRIPTS_DIR . '/variable_product.js', [], '1.0.0', true);


    wp_enqueue_style('comes-e-bebes-reset');
    wp_enqueue_style('comes-e-bebes-fonts');
    wp_enqueue_style('style');
    wp_enqueue_style('comes-e-bebes-header');
    wp_enqueue_style('page-home');
    wp_enqueue_style('comes-e-bebes-pagehome');
    wp_enqueue_style('comes-e-bebes-footer');
    wp_enqueue_style('comes-e-bebes-nav');
    wp_enqueue_style('comes-e-bebes-cart');
    wp_enqueue_style('comes-e-bebes-archive');
    wp_enqueue_style('comes-e-bebes-adress');
    wp_enqueue_style('comes-e-bebes-painel');
    wp_enqueue_style('comes-e-bebes-nav-myaccount');
    wp_enqueue_style('comes-e-bebes-orders');
    wp_enqueue_style('comes-e-bebes-single-product');
    wp_enqueue_style('comes-e-bebes-checkout');

    wp_enqueue_script('comes-e-bebes-header-js');
    wp_enqueue_script('comes-e-bebes-galeria-js');
    wp_enqueue_script('comes-e-bebes-search-dishe-js');
    wp_enqueue_script('comes-e-bebes-cart-js');
    wp_enqueue_script('comes-e-bebes-simple-product-js');
    wp_enqueue_script('comes-e-bebes-variable-product-js');
}

?>