<?php

    function custom_default_address_fields( $fields ) {
        // Only on account pages
        if( ! is_account_page() ) return $fields;

        /*Remover os campos*/
        unset($fields['address_2']);
        unset($fields['company']);
        unset($fields['state']);
        
        /*Campo Nome */
        $fields[ 'first_name' ][ 'placeholder' ] = 'Digite seu nome';
        $fields[ 'first_name' ][ 'priority' ] = 10;

        /*Campo Sobrenome */
        $fields[ 'last_name' ][ 'placeholder' ] = 'Digite seu sobrenome';
        $fields[ 'last_name' ][ 'priority' ] = 20;

        /*Campo Cep */
        $fields[ 'postcode' ][ 'placeholder' ] = 'Digite seu CEP';
        $fields[ 'postcode' ][ 'priority' ] = 30;

        /*Campo Endereco */
        $fields[ 'address_1' ][ 'label' ] = 'Logradouro';
        $fields[ 'address_1' ][ 'placeholder' ] = 'Rua e número';
        $fields[ 'address_1' ][ 'priority' ] = 40;

        /*Campo Complemento */
        $fields['complemento']['label'] = 'Complemento';
        $fields['complemento']['class'] = array( 'form-row-wide' );
        $fields['complemento']['placeholder'] = 'Seu Complemento';
        $fields['complemento']['priority'] = 50;

        /*Campo Cidade */
        $fields[ 'city' ][ 'placeholder' ] = 'Sua cidade';
        $fields[ 'city' ][ 'class' ] = array( 'form-row-last' );
        $fields['city']['priority'] = 60;

        /*Campo Bairro */
        $fields['bairro']['label'] = 'Bairro';
        $fields['bairro']['class'] = array( 'form-row-first' );
        $fields['bairro']['priority'] = 'Bairro';
        $fields['bairro']['placeholder'] = 'Seu Bairo';
        $fields['bairro']['priority'] = 70;

        return $fields;
    }

?>