<?php 

function formatar_produto($produto){
    $id_produto = $produto->get_id();
    $img_produto_url = wp_get_attachment_url(get_post_thumbnail_id( $id_produto ));
    $css_image_product = 'background-image: url('. $img_produto_url .');';
    ?>

    <div class="dishe" style="<?= $css_image_product;?>">
        <div class="name-price">
            <h4><?= $produto->get_name();?></h4>
            <div class="buy-dishe">
                <h3><?= $produto->get_price_html(); ?></h3>
                <a class="cart" href="<?= $produto->get_permalink();?>"><img src="<?= IMAGES_DIR . '/cart-product.png';?>" alt="Icone do carrinho"></a>
            
            </div>
        </div>
    </div>

    <?php  
}
?>