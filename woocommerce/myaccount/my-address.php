<?php

defined( 'ABSPATH' ) || exit;

$customer_id = get_current_user_id();

if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) {
	$get_addresses = apply_filters(
		'woocommerce_my_account_get_addresses',
		array(
			'shipping' => __( 'Shipping address', 'woocommerce' ),
		),
		$customer_id
	);
} else {
	$get_addresses = apply_filters(
		'woocommerce_my_account_get_addresses',
		array(
			'billing' => __( 'Billing address', 'woocommerce' ),
		),
		$customer_id
	);
}

?>

<p>
	<?php echo apply_filters( 'woocommerce_my_account_my_address_description', esc_html__( 'The following addresses will be used on the checkout page by default.', 'woocommerce' ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
</p>

<?php if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) : ?>
	<div class="u-columns woocommerce-Addresses col2-set addresses">
<?php endif; ?>

<?php foreach ( $get_addresses as $name => $address_title ) : ?>
	<?php
		$address = wc_get_account_formatted_address( $name );

	?>

	<div class="address">
		<p class="description">Os endereços a seguir serão usados na página de finalizar pedido como 
			endereços padrões, mas é possível modificá-los durante a 
			finalização do pedido.
		</p>
		<div class="container_address">
			<h3 class="c"><?php echo esc_html( $address_title ); ?></h3>
			<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', $name) ); ?>" class="titulo_container_link">Adcionar Endereço</a>
		</div>	
		<ul class="lista_cliente">
			<li class="cliente">
				<p class="cliente_nome"><?php echo get_user_meta( $customer_id, $name . '_first_name', true ) .' '. get_user_meta( $customer_id, $name . '_last_name', true ); ?></p>
				<p class="cliente_cep"><?php echo get_user_meta( $customer_id, $name . '_postcode', true ); ?></p>
				<div class="container_endereco_entrega">
					<p class="cliente_endereco_completo"><?php echo get_user_meta( $customer_id, $name . '_address_1', true )."," . get_user_meta( $customer_id, $name . '_complemento', true ).', ' .get_user_meta( $customer_id, $name . '_bairro', true ); ?></p>
					<p class="cliente_endereco_cidade"><?php echo get_user_meta( $customer_id, $name . '_city', true )."<br>"; ?></p>
					<p class="cliente_endereco_estado"><?php echo WC()->countries->states['BR'][get_user_meta( $customer_id, $name . '_state', true )]."<br>"; ?></p>
				</div>
				<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', $name ) ); ?>" class="cliente_editar">Editar</a>
			</li>
		</ul>		
	</div>

<?php endforeach; ?>

<?php if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) : ?>
	</div>
	<?php
endif;
