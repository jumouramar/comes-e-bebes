<?php
get_header();
?>

	<main class="main-page-archive">
		<div class="main-content-archive">
			<h1>SELECIONE UMA CATEGORIA</h1>
			<div class="categories">
				<?php
					do_action( 'available_categories', 'mostrar_categorias');
				?>
			</div>

			<div class="type-dish">
				<h2>PRATOS</h2>
				<p><?php echo get_selected_category() ?></p>
			</div>
			
			<div class="search-div">
				<form action="<?php bloginfo('url');?>/shop/" method="get" class="search-form">
					<div class="search-name">
						<label>Buscar por nome:</label>
						<input type="text" name='s'>
					</div>

					<div class="sort-attributes">
						<div class="sort-options">
							<label>Ordenar por:</label>
							<select name="orderby" id="sort-options" class="orderby">
								<option value="1"></option>
								<option value="price">Preço - crescente</option>
								<option value="price-desc">Preço - decrescente</option>
								<option value="rating">Classificação - crescente</option>
								<option value="rating-desc">Classificação - decrescente</option>
							</select>
						</div>
					</div>
					
					<input type="submit" value="Buscar" class="button-buscar">
				</form>

				<form class="sort-price" action="?" method="get">
					<p>Filtro de preço</p>
					<div class="price-margin">
						<div class="less-price">
							<label for="min_price">De:</label>
							<input type="text" id="min_price" name="min_price">
						</div>
						<div class="biggest-price">
							<label for="max_price">Até:</label>
							<input type="text" id="max_price" name="max_price">
						</div>
					</div>

					<input class="button-buscar" type="submit" value="Buscar">
				</form>
			</div>

			<div class="dishes-list">
				<?php

				$min_price = ( isset( $_GET['min_price'] ) ) ? intval( $_GET['min_price'] ) : '';
				$max_price = ( isset( $_GET['max_price'] ) ) ? intval( $_GET['max_price'] ) : '';

				$args = array(
				'post_type' => 'product',
				'posts_per_page' => -1,
				'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => '_price',
					'value' => $min_price,
					'compare' => '>=',
					'type' => 'NUMERIC'
				),
				array(
					'key' => '_price',
					'value' => $max_price,
					'compare' => '<=',
					'type' => 'NUMERIC'
				)
				)
				);

				$query = new WP_Query( $args );
				
				if (have_posts() ) {
					
					while (have_posts() ) {
						the_post();
						formatar_produto(wc_get_product(get_the_ID()));
					}
					wp_reset_postdata();
				}
				else{
					?>
					<script>
						var voltarLoja = document.querySelector('.right-menu button');
						voltarLoja.click();
						alert('Não foi encontrado nenhum produto');
					</script>
					<?php  
				}
				
				?>
			</div>
			<div class="pagination-products">
				<?php echo get_the_posts_pagination(array(
					'prev_text' => '<',
					'next_text' =>  '>',
				));?>
			</div>

		</div>
	</main>


<?php
get_footer();
?>