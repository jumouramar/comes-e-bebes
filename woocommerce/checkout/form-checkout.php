<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>
		
		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col1">
			<div class="biling-details" id="customer_details">
				<h3>CONFIRMAÇÃO DE PEDIDO</h3>
				<h4>Informações de entrega</h4>
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>
                <h4>Informações de pagamento</h4>
                <h5>Forma de Pagamento</h5>
			<div class="payment-method">
                <button class="btn-money">
                    <img src="<?php echo IMAGES_DIR . '/truc.png'?>" alt="Caminhão">
                    <div>
                        <p>Dinheiro</p>
                        <p>Na entrega</p>
                    </div>
                </button>
                <button class="btn-card">
                    <img src="<?php echo IMAGES_DIR . '/card.png'?>" alt="Cartão">
                    <p>Cartão</p>
                </button>
			</div>
		</div>
		

	<?php endif; ?>
	
	<div class="line"></div>
	<div class="checkout-col2">
		<h3>PEDIDOS</h3>
        <div>
            <?php show_checkout_itens(); ?>
        </div>
        <div class="checkout-subtotal-shipping">
            <div class="checkout-subtotal">
                <p>Subtotal:</p>
                <p><?php wc_cart_totals_subtotal_html(); ?></p> 
            </div>
            <div class="checkout-shipping">
                <p>Entrega:</p>
                <p><?php  wc_cart_totals_shipping_html();?></p>
            </div>
        </div>
        <div class="checkout-total">
            <p>Total:</p>
            <p><?php  wc_cart_totals_order_total_html();?></p>
        </div>
        
        <div class="checkout-btn">
            <a href="<?php bloginfo('url');?>/checkout/"><button>CONFIRMAR</button></a>
        </div>
    </div>     
</form>

<!-- FUNÇÃO PARA FORMATAR OS ITENS -->
<?php 
function show_checkout_itens(){

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
        ?>
        <div class="product-checkout">
            <div class="img-cart">
                <?= $_product->get_image(); ?>
            </div>
            <div class="name-cart">
                <p id="p-checkout"><?= $_product->get_name(); ?></p>
                <div class="qtd-price-cart">
                    <div class="qtd-cart">
                        <div><p id="p-cart-1">-</p></div>
                        <div><p id="p-cart-2"><?= $cart_item['quantity']; ?></p></div>
                        <div><p id="p-cart-3">+</p></div>
                    </div>
                    <p>R$ <?= $_product->get_price() * $cart_item['quantity']; ?></p>  
                </div>
            </div>
        </div>
        <?php
        }
    }
}
?>

<!-- FUNÇÃO PARA DEFIFNIR OS FILDS DA CONFIRMAÇÃO DE PEDIDO -->
<?php
function custom_override_checkout_fields($fields){
    $fields['order'] = [];
    $fields['billing'] = [];
    $fields['account'] = [];
    $fields['shipping'] = [];

    $fields['billing']['billing_email'] = [
        'label' => 'Email para contato',
        'required' => true,
        'class' => 'billing-mail'
    ];
    $fields['billing']['billing_first_name'] = [
        'label' => 'Nome',
        'required' => true,
        'class' => 'billing-name'
    ];
    $fields['billing']['billing_last_name'] = [
        'label' => 'Sobrenome',
        'required' => true,
        'class' => 'billing-last-name'
    ];
    $fields['billing']['billing_phone'] = [
        'label' => 'Telefone fixo',
        'required' => true,
        'class' => 'billing-phone'
    ];
    $fields['billing']['billing_cellphone'] = [
        'label' => 'Celular',
        'required' => true,
        'class' => 'billing-cellphone'
    ];
    $fields['billing']['billing_cep'] = [
        'label' => 'CEP',
        'required' => true,
        'class' => 'billing-cep'
    ];
    $fields['billing']['billing_logradouro'] = [
        'label' => 'Logradouro',
        'required' => true,
        'class' => 'billing-logradouro'
    ];
    $fields['billing']['billing_complement'] = [
        'label' => 'Complemento',
        'required' => true,
        'class' => 'billing-complement'
    ];
    $fields['billing']['billing_neighborhood'] = [
        'label' => 'Bairro',
        'required' => true,
        'class' => 'billing-neighborhood'
    ];
    $fields['billing']['billing_city'] = [
        'label' => 'Cidade',
        'required' => true,
        'class' => 'billing-city'
    ];
    return $fields;

}
?>