<?php

$idCurrentProduct = get_the_ID();
$currentProduct = wc_get_product( $idCurrentProduct );
$productCategory = $currentProduct->get_category_ids()[0];


$slugCategory = get_term_by( 'id', $productCategory, 'product_cat' )->slug;

$args = array(
    'limit' => 4,
    'category' => array( $slugCategory ),
);

$productsRelated = wc_get_products( $args );
?>
 
<div class="products-related">
    <?php if(get_term_by( 'id', $productCategory, 'product_cat' )->slug == 'massa'){
        echo '<h2>MAIS '. strtoupper(get_term_by( 'id', $productCategory, 'product_cat' )->name) . '</h2>';
    } 
    else{
        echo '<h2>MAIS COMIDA ' . strtoupper(get_term_by( 'id', $productCategory, 'product_cat' )->name) . '</h2>';
    }
    ?>

    <div class="dishes-list">
        <?php
        foreach($productsRelated as $related){
            formatar_produto($related);
        }
    
        ?>
    </div>
</div>