<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); 
		the_title( '<div class="name-description-simple"> <h1 class="title-single-product product_title entry-title">', '</h1>' );
		$idCurrentProduct = get_the_ID();
		$currentProduct = wc_get_product( $idCurrentProduct );
		$descriptionCurrent = $currentProduct->get_description();
		?>
		<?php echo '<p>' . $descriptionCurrent . '</p>'; ?>
	</div>
		
	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); 
		
		?>
		
		<div class="info-buy-simple">
			<div class="quantity-price-simple">
				<?php
				do_action( 'woocommerce_before_add_to_cart_quantity' );  
				?>
				<div class="change-quantity">
					<button type="button" class="button-quantity button-quantity-decrease">-</button>
					<?php
					woocommerce_quantity_input(
						array(
							'min_value'   => 1,
							'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
							'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
						)
					);
					?>
					<button type="button" class="button-quantity button-quantity-increase">+</button>
				</div>
				<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>"><?php echo $product->get_price_html(); ?></p>
				<?php
				do_action( 'woocommerce_after_add_to_cart_quantity' );

				?>
			</div>
		<div>

			<div class="variation-button-simple">
				<select name="" id="" class="select-simple">
					<option value="">Não há variação de porção</option>
				</select>
				<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt<?php echo esc_attr( wc_wp_theme_get_element_class_name( 'button' ) ? ' ' . wc_wp_theme_get_element_class_name( 'button' ) : '' ); ?>">ADICIONAR</button>
			</div>
		</div>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>
	</div>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
