<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- FONTS -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <!--       -->
    <title><?php bloginfo( 'name' ); ?> | <?php the_title(); ?></title>
    <?php wp_head(); ?>
</head>
<body>
    <header class="header">
        <div class="left-menu">
            <?php the_custom_logo(); ?>
            <div class="search">
                <form action="<?php bloginfo('url');?>" method="get">
                    <img class="search-icon" src="<?php echo IMAGES_DIR . '/search-vector.png'?>" alt="Lupa">
                    <input type="text" name="s" id="s" placeholder="Sashimi.." class="search-input">
                    <input type="hidden" name="post_type" value="product">
                </form>
            </div>       
        </div>
        <div class="right-menu">
            <a href="<?php bloginfo('url');?>/shop/"><button>Faça um pedido</button></a>
            <img class="cart-icon" src="<?php echo IMAGES_DIR . '/cart-vector.png'?>" alt="Carrinho"></a>
            <a href="<?php bloginfo('url');?>/my-account/"><img class="profile-icon" src="<?php echo IMAGES_DIR . '/profile-vector.png'?>" alt="Perfil"></a>
        </div>    
        <span class="burguer-menu"><img class="menu-icon" src="<?php echo IMAGES_DIR . '/menu.png'?>" alt="Menu"></span>
        <div class="burguer-menu-content">
            <span>
                <a href="<?php bloginfo('url');?>/shop/">Faça um pedido</a>
            </span>
            <span>
                <p class="link-cart">Carrinho</p>
            </span>
            <span>
                <a href="<?php bloginfo('url');?>/my-account/">Perfil</a>
            </span>
        </div>
    </header>
        
   <!-- CARRINHO -->
   <section class="lateral-cart">
        <div class="smoke"></div>
        <div class="lateral-modal">
            <div class="modal-content">
                <div class="modal-exit"><img class="x-icon" src="<?php echo IMAGES_DIR . '/x.png'?>" alt="Saida"></div>
                <h1>CARRINHO</h1>  
                <div class="modal-itens">
                    <?php show_cart_itens(); ?>
                </div>
                <p>Total do carrinho: <?php wc_cart_totals_subtotal_html(); ?></p>
                <div class="cart-btn">
                    <a href="<?php bloginfo('url');?>/checkout/"><button>COMPRAR</button></a>
                </div>
            </div>
        </div>
    </section>


    <?php 
    function show_cart_itens(){

        foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

            if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {

            }?>
            
            <div class="product-cart">
                <div class="img-cart">
                    <?= $_product->get_image(); ?>
                </div>
                <div class="name-cart">
                    <p><?= $_product->get_name(); ?></p>
                    <div class="qtd-price-cart">
                        <div class="qtd-cart">
                            <div><p id="p-cart-1">-</p></div>
                            <div><p id="p-cart-2"><?= $cart_item['quantity']; ?></p></div>
                            <div><p id="p-cart-3">+</p></div>
                        </div>
                        <p>R$ <?= $_product->get_price() * $cart_item['quantity']; ?></p>  
                    </div>
                </div>
            </div>
            
            
            <?php
        }
    }
    ?>