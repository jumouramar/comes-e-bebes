<?php

// variaveis
define('ROOT_DIR', get_theme_file_path());
define('STYLES_DIR', get_template_directory_uri() . '/assets/css');
define('IMAGES_DIR', get_template_directory_uri() . '/assets/images');
define('INCLUDES_DIR', ROOT_DIR . '/includes');
define('SCRIPTS_DIR', get_template_directory_uri() . '/assets/js');

// includes
include_once(INCLUDES_DIR . '/enqueue.php');
include_once(INCLUDES_DIR . '/setup-theme.php');
include_once(INCLUDES_DIR . '/format-categories.php');
include_once(INCLUDES_DIR . '/format-product.php');
include_once(INCLUDES_DIR . '/get-selected-category.php');
include_once(INCLUDES_DIR . '/format-address-form.php');



// ganchos
add_action('wp_enqueue_scripts', 'comes_e_bebes_enqueue_style');
add_action('after_setup_theme', 'comes_e_bebes_setup_theme');
add_action( 'available_categories', 'mostrar_categorias');
add_filter('loop_shop_per_page', 'comes_e_bebes_per_page');
   
add_filter( 'woocommerce_account_menu_items', 'bbloomer_add_link_my_account' );


function bbloomer_add_link_my_account( $items ) {
    $newitems = array(
       'dashboard'       => __( 'Dashboard', 'woocommerce' ),
       'orders'          => __( 'Orders', 'woocommerce' ),
       'edit-address'    => _n( 'Addresses', 'Address', (int) wc_shipping_enabled(), 'woocommerce' ),
       'customer-logout' => __( 'Logout', 'woocommerce' )
     );   
    return $newitems;
 }

    // checkout
add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');
add_filter(  'woocommerce_default_address_fields', 'custom_default_address_fields', 20, 1 );
?>